package com.lcd;

import com.lcd.utils.LcdDigitFactory;
import com.lcd.utils.NumberFormatChecker;
import com.lcd.utils.UserInputGridConverter;
import java.util.Scanner;

public class AppLauncher {

  public static void main(final String[] args) {

    final UserInputGridConverter userInputGridConverter = new UserInputGridConverter(
        new LcdDigitFactory(), new NumberFormatChecker());
    final Lcd lcd = new Lcd(userInputGridConverter);

    // Scanner in a try-with-resources to be automatically closed!
    try (final Scanner in = new Scanner(System.in)) {

      String userEntry = promptUserAndGetEntry(in);
      while (!"q".equalsIgnoreCase(userEntry)) {
        lcd.processUserEntry((userEntry));
        userEntry = promptUserAndGetEntry(in);
      }
      System.out.println("Quit application");
    }
  }

  private static String promptUserAndGetEntry(final Scanner in) {
    System.out.print("\nEnter a integer value to display or 'q|Q' to quit the application: ");
    return in.nextLine();
  }

}


