package com.lcd.constants;

public final class GridConstants {

  private GridConstants() {
    // Utility class
  }

  public static final String DOT = ".";
  public static final String UNDERSCORE = "_";
  public static final String PIPE = "|";

  public static final String[][] ZERO = {{DOT, UNDERSCORE, DOT}, {PIPE, DOT, PIPE},
      {PIPE, UNDERSCORE, PIPE}};
  public static final String[][] ONE = {{DOT, DOT, DOT}, {DOT, DOT, PIPE}, {DOT, DOT, PIPE}};
  public static final String[][] TWO = {{DOT, UNDERSCORE, DOT}, {DOT, UNDERSCORE, PIPE},
      {PIPE, UNDERSCORE, DOT}};
  public static final String[][] THREE = {{DOT, UNDERSCORE, DOT}, {DOT, UNDERSCORE, PIPE},
      {DOT, UNDERSCORE, PIPE}};
  public static final String[][] FOUR = {{DOT, DOT, DOT}, {PIPE, UNDERSCORE, PIPE},
      {DOT, DOT, PIPE}};
  public static final String[][] FIVE = {{DOT, UNDERSCORE, DOT}, {PIPE, UNDERSCORE, DOT},
      {DOT, UNDERSCORE, PIPE}};
  public static final String[][] SIX = {{DOT, UNDERSCORE, DOT}, {PIPE, UNDERSCORE, DOT},
      {PIPE, UNDERSCORE, PIPE}};
  public static final String[][] SEVEN = {{DOT, UNDERSCORE, DOT}, {DOT, DOT, PIPE},
      {DOT, DOT, PIPE}};
  public static final String[][] HEIGHT = {{DOT, UNDERSCORE, DOT}, {PIPE, UNDERSCORE, PIPE},
      {PIPE, UNDERSCORE, PIPE}};
  public static final String[][] NINE = {{DOT, UNDERSCORE, DOT}, {PIPE, UNDERSCORE, PIPE},
      {DOT, DOT, PIPE}};
}
