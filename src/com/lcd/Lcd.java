package com.lcd;

import com.lcd.exceptions.InvalidUserInputException;
import com.lcd.model.LdcScreen;
import com.lcd.utils.UserInputGridConverter;

public class Lcd {

  private final UserInputGridConverter userInputGridConverter;

  public Lcd(final UserInputGridConverter userInputGridConverter) {
    this.userInputGridConverter = userInputGridConverter;
  }

  public void processUserEntry(final String userEntry) {
    try {
      final LdcScreen ldcScreen = userInputGridConverter.toLcdScreen(userEntry);
      ldcScreen.display();
    } catch (final InvalidUserInputException e) {
      System.out.println("ERROR - Invalid input: must be only positive integers!");
    }
  }
}


