package com.lcd.utils;

public final class NumberFormatChecker {

  public boolean isNumeric(final String strNum) {
    if (strNum == null) {
      return false;
    }
    try {
      final double d = Long.parseLong(strNum);
    } catch (final NumberFormatException nfe) {
      return false;
    }
    return true;
  }
}
