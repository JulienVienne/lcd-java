package com.lcd.utils;

import com.lcd.exceptions.InvalidUserInputException;
import com.lcd.model.LdcScreen;
import java.util.Arrays;
import java.util.stream.Collectors;

public final class UserInputGridConverter {

  private final LcdDigitFactory lcdDigitFactory;
  private final NumberFormatChecker numberFormatChecker;

  public UserInputGridConverter(final LcdDigitFactory lcdDigitFactory,
      final NumberFormatChecker numberFormatChecker) {
    this.lcdDigitFactory = lcdDigitFactory;
    this.numberFormatChecker = numberFormatChecker;
  }

  public LdcScreen toLcdScreen(final String userInput) {

    final LdcScreen result = Arrays.asList(userInput.split(""))
        .stream()
        .peek(s -> {
          if (!numberFormatChecker.isNumeric(s)) {
            throw new InvalidUserInputException();
          }
        })
        .map(Integer::valueOf)
        .map(lcdDigitFactory::toLcdDigit)
        .collect(Collectors.toCollection(LdcScreen::new));
    return result;
  }
}
