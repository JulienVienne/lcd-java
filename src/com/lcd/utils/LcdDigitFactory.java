package com.lcd.utils;

import com.lcd.constants.GridConstants;
import com.lcd.model.LcdDigit;

public final class LcdDigitFactory {

  public LcdDigit toLcdDigit(final int number) {
    String[][] value = null;
    switch (number) {
      case 0:
        value = GridConstants.ZERO;
        break;
      case 1:
        value = GridConstants.ONE;
        break;
      case 2:
        value = GridConstants.TWO;
        break;
      case 3:
        value = GridConstants.THREE;
        break;
      case 4:
        value = GridConstants.FOUR;
        break;
      case 5:
        value = GridConstants.FIVE;
        break;
      case 6:
        value = GridConstants.SIX;
        break;
      case 7:
        value = GridConstants.SEVEN;
        break;
      case 8:
        value = GridConstants.HEIGHT;
        break;
      case 9:
        value = GridConstants.NINE;
        break;
      default:
        throw new RuntimeException("Can only convert digits from 0 to 9");
    }
    return new LcdDigit(value);

  }
}
