package com.lcd.model;

import java.util.ArrayList;

public class LdcScreen extends ArrayList<LcdDigit> {

  private static final long serialVersionUID = -424971245280529832L;

  public void display() {
    for (int i = 0; i < 3; ++i) {
      for (int k = 0; k < this.size(); ++k) {
        for (int j = 0; j < 3; ++j) {
          System.out.print(this.get(k).getValue()[i][j]);
        }
        System.out.print(" ");
      }
      System.out.println();
    }
  }
}
