package com.lcd.model;

public final class LcdDigit {

  private String[][] value;

  public LcdDigit(final String[][] value) {
    this.value = value;
  }

  public String[][] getValue() {
    return value;
  }
}
