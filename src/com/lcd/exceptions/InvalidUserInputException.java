package com.lcd.exceptions;

public class InvalidUserInputException extends RuntimeException {

  private static final long serialVersionUID = 6825347792443742629L;

  public InvalidUserInputException() {
    super("User input must be an integer value");
  }

}
